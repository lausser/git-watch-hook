FROM alpine/git:v2.32.0
LABEL org.opencontainers.image.authors="Gerhard Lausser"
LABEL description="Watch a git repository and call a webhook on changes"

USER root
RUN apk update
RUN apk add --no-cache python3
RUN apk add --no-cache py3-pip
RUN pip install gitpython
RUN mkdir -p /git-hashes
WORKDIR /root
ENTRYPOINT ["/root/run.py"]
ADD run.py /root
RUN chmod 755 /root/run.py
ADD VERSION /root
