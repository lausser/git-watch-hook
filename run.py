#! /usr/bin/python3

import git
import requests
import sys
import os
import re
import time
import json
import argparse

# GET url ob exists
#my_git_path = "https://github.com/consol/omd"
#my_git_branch = "labs"

def is_accessible(remote_repo_url):
    if remote_repo_url.startswith("http"):
        r = requests.get(remote_repo_url, auth=('user', 'pass'))
        if not r.status_code == 200:
            return False, "remote url {} is inaccessible".format(remote_repo_url)
    elif remote_repo_url.startswith("/"):
        if not os.path.exists(remote_repo_url):
            return False, "remote dir {} is inaccessible".format(remote_repo_url)
    return True, None

def local_repo_folder(remote_repo_url, root, dest):
    if dest:
        return os.path.join(root, dest)
    else:
        return os.path.join(root, re.sub(r'[^0-9a-zA-Z]+', '_', remote_repo_url))

def create_empty_repo(remote_repo_url, repo_folder):
    if os.path.exists(repo_folder):
        empty_repo = git.Repo(repo_folder)
    else:
        empty_repo = git.Repo.init(repo_folder)
        origin = empty_repo.create_remote('origin', remote_repo_url)
    return empty_repo

def get_saved_hashes(folder):
    saved_hashes = {}
    try:
        with open(folder+".json") as fp:
            saved_hashes = json.load(fp)
    except Exception:
        pass
    return saved_hashes

def save_hashes(hashes, folder):
    with open(folder+".json", "w") as fp:
        json.dump(hashes, fp)

def get_remote_hashes(repo):
    hashes = {}
    lines = repo.git.ls_remote("--heads")
    for line in lines.split("\n"):
        hash, ref = re.split(r'\s+', line)
        ref = ref.split("/")[2]
        hashes[ref] = hash
    return hashes



if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--repo', type=str, default=os.getenv('GIT_SYNC_REPO', None), help='the git repository to clone')
    parser.add_argument('--branch', type=str, default=os.getenv('GIT_SYNC_BRANCH', "master"), help='the git branch to check out')
    parser.add_argument('--rev', type=str, default=os.getenv('GIT_SYNC_REV', "HEAD"), help='the git revision (tag or hash) to check out')
    parser.add_argument('--depth', type=int, default=os.getenv('GIT_SYNC_DEPTH', 0), help='use a shallow clone with a history truncated to the specified number of commits')
    parser.add_argument('--root', type=str, default=os.getenv('GIT_SYNC_ROOT', os.getenv("HOME")+"/git"), help='the root directory for git-sync operations, under which --dest will be created')
    parser.add_argument('--dest', type=str, default=os.getenv('GIT_SYNC_DEST', None), help='the name of (a symlink to) a directory in which to check-out files under --root (defaults to the leaf dir of --repo)')
    parser.add_argument('--wait', type=int, default=os.getenv('GIT_SYNC_WAIT', 1), help='the number of seconds between syncs')
    parser.add_argument('--timeout', type=int, default=os.getenv('GIT_SYNC_TIMEOUT', 120), help='the max number of seconds allowed for a complete sync')
    parser.add_argument('--change-permissions', type=int, default=os.getenv('GIT_SYNC_PERMISSIONS', 0), help='the file permissions to apply to the checked-out files (0 will not change permissions at all)')
    parser.add_argument('--sparse-checkout-file', type=str, default=os.getenv('GIT_SYNC_SPARSE_CHECKOUT_FILE', None), help='the location of an optional sparse-checkout file, same syntax as a .gitignore file.')
    parser.add_argument('--sync-hook-command', type=str, default=os.getenv('GIT_SYNC_HOOK_COMMAND', None), help='DEPRECATED: use --exechook-command instead')
    parser.add_argument('--exechook-timeout', type=int, default=os.getenv('GIT_SYNC_EXECHOOK_TIMEOUT', 30), help='the timeout for the sync hook command')
    parser.add_argument('--webhook-url', type=str, default=os.getenv('GIT_SYNC_WEBHOOK_URL', None), help='the URL for a webook notification when syncs complete')
    parser.add_argument('--webhook-method', type=str, default=os.getenv('GIT_SYNC_WEBHOOK_METHOD', "POST"), help='the HTTP method for the webhook')
    parser.add_argument('--webhook-timeout', type=int, default=os.getenv('GIT_SYNC_WEBHOOK_TIMEOUT', 1), help='the timeout for the webhook')
    parser.add_argument('--webhook-backoff', type=int, default=os.getenv('GIT_SYNC_WEBHOOK_BACKOFF', 3), help='the time to wait before retrying a failed webhook')
    parser.add_argument('--username', type=str, default=os.getenv('GIT_SYNC_USERNAME', None), help='the username to use for git auth')
    parser.add_argument('--password', type=str, default=os.getenv('GIT_SYNC_PASSWORD', None), help='the password or personal access token to use for git auth. (users should prefer --password-file or env vars for passwords)')
    parser.add_argument('--password-file', type=str, default=os.getenv('GIT_SYNC_PASSWORD_FILE', None), help='the path to password file which contains password or personal access token (see --password)')
    parser.add_argument('--ssh-key-file', type=str, default=os.getenv('GIT_SSH_KEY_FILE', "/etc/git-secret/ssh"), help='the SSH key to use')
    parser.add_argument('--ssh-known-hosts-file', type=str, default=os.getenv('GIT_SSH_KNOWN_HOSTS_FILE', "/etc/git-secret/known_hosts"), help='the known_hosts file to use')
    parser.add_argument('--askpass-url', type=str, default=os.getenv('GIT_ASKPASS_URL', None), help='the URL for GIT_ASKPASS callback')
    parser.add_argument('--git', type=str, default=os.getenv('GIT_SYNC_GIT', "git"), help='the git command to run (subject to PATH search, mostly for testing')
    args = parser.parse_args()
    while True:
        print("loop")
        accessible, reason = is_accessible(args.repo)
        if accessible:
            repo_folder = local_repo_folder(args.repo, args.root, args.dest)
            saved_hashes = get_saved_hashes(repo_folder)
            repo = create_empty_repo(args.repo, repo_folder)
            if repo:
                remote_hashes = get_remote_hashes(repo)
                if args.branch in remote_hashes:
                    if args.branch not in saved_hashes or remote_hashes[args.branch] != saved_hashes[args.branch]:
                        print("was updated")
                        saved_hashes[args.branch] = remote_hashes[args.branch]
                    else:
                        pass
                else:
                    print("bad branch")
    
                for hash in saved_hashes:
                    if hash not in remote_hashes:
                        del saved_hashes[hash]
                save_hashes(saved_hashes, repo_folder)
        else:
            print(reason)
        time.sleep(args.wait)

